SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE `duyurular` (
  `duyuru_id` int(11) NOT NULL AUTO_INCREMENT,
  `baslik` text NOT NULL,
  `icerik` text NOT NULL,
  `tarih` text NOT NULL,
  PRIMARY KEY (`duyuru_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin5 AUTO_INCREMENT=4 ;

INSERT INTO `duyurular` (`duyuru_id`, `baslik`, `icerik`, `tarih`) VALUES
(2, 'Test Duyurusu', '<p>Bu bir test duyurusu.</p>', '21.05.2014'),
(3, 'Test Duyurusu 2', '<p>Bu başka bir test duyurusu</p>', '21.05.2014');

CREATE TABLE `kategoriler` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_adi` text NOT NULL,
  `aciklama` text NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin5 AUTO_INCREMENT=4 ;

INSERT INTO `kategoriler` (`kategori_id`, `kategori_adi`, `aciklama`) VALUES
(2, 'Test Kategori 1', 'Bu bir test kategorisidir.'),
(3, 'Test Kategori 2', 'Bu da başka bir test kategorisi!');

CREATE TABLE `sabitler` (
  `hakkimizda` text NOT NULL,
  `vizyon` text NOT NULL,
  `misyon` text NOT NULL,
  `iletisim` text NOT NULL,
  `telefon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin5;

INSERT INTO `sabitler` (`hakkimizda`, `vizyon`, `misyon`, `iletisim`, `telefon`) VALUES
('<center>\r\n\r\n        <div class="page-container">\r\n\r\n            <div style="color:#f47925; font-size:15px; font-weight:bold">Hakkımızda</div>\r\n            <div style="margin-top:12px; margin-bottom:30px;"><div class="PageBody">\r\n                    <p><strong><span style="font-size: medium;">MSEMedikal.com&nbsp; MSE Medikal Ürünleri ve Özel Sağlık Hizmetleri Sanayi ve Ticaret İthalat ve İhracat Ltd. Şti. ne ait bir sitedir. </span></strong></p> \r\n                    <p><strong><span style="font-size: medium;">Sağlık sektöründe faaliyet gösteren MSE MEDİKAL sürekli olarak kaliteyi üst seviyelere taşıyıp tüm Türkiye sınırlarında&nbsp;medikal ürünleri en uygun fiyatla, en kısa sürede tüketicilere ulaştırmayı hedeflemektedir. </span></strong></p> \r\n                    <p><strong><span style="font-size: medium;">Sitemizdeki tüm ürünler ithalatçı firma garantisi altındadır.</span></strong></p> \r\n                    <p><strong><span style="font-size: medium;">MSE MEDİKAL , sağlık sektöründeki kuruluşların ana görevinin, insan yaşamının kalitesini ve süresini arttırmak olduğunun bilinciyle en son teknolojilerin ülkemizde kullanılması ve yaygınlaşması için tüm Türkiye''de&nbsp;aralıksız çalışmaktadır. Sağlık sektöründeki eğitimin önemini kavramış olan firmamız tüm çalışanlarına sektördeki gerekli eğitimleri aldırarak bu konuda da öncü olmayı hedeflemektedir.</span></strong></p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </center>', '<h1 style="text-align: center;"><strong><span style="color: #f47925; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 15px; text-align: -webkit-center;">Vizyonumuz</span></strong></h1>\r\n<p style="text-align: center;"><strong>Yenilik&ccedil;i, akılcı, ilkeli ve sorumlu yaklaşımımızla b&ouml;lgemizde lider olmak.</strong></p>\r\n<p style="text-align: center;"><strong>MSE Medikal her ge&ccedil;en yıl insanlarımıza daha iyi ve daha uygun hizmet verebilmek i&ccedil;in &ccedil;alışmaya devam edecek...</strong></p>\r\n<p style="text-align: center;"><strong>İnsan sağlığı ve insan hayatı bizim i&ccedil;in &ouml;nde gelir.</strong></p>', '<h2 style="text-align: center;">Misyonumuz</h2>\r\n<div>\r\n<div class="PageBody">\r\n<p style="text-align: center;"><strong>Saglık ihmale gelmez.</strong></p>\r\n<p style="text-align: center;"><strong>MSE Medikal insanlara sağlık sekt&ouml;r&uuml;nde daha iyi hizmet verebilmek i&ccedil;in kurulmustur.</strong></p>\r\n<p style="text-align: center;"><strong>İnsan sağlığı ve insan hayatı bizim i&ccedil;in &ouml;nde gelir.</strong></p>\r\n</div>\r\n</div>', '<h2 style="padding-left: 60px;">İletişim Bilgilerimiz :</h2>\r\n<p style="padding-left: 60px;">Dr. Sadık Ahmet Cd. S&ouml;ğ&uuml;t Sitesi</p>\r\n<p style="padding-left: 60px;">B-29 Blok, Kat 5, Daire 9</p>\r\n<p style="padding-left: 60px;">31000 Iskenderun / HATAY</p>\r\n<p style="padding-left: 60px;">&nbsp;</p>\r\n<p style="padding-left: 60px;">+90 326 612 34 56</p>\r\n<p style="padding-left: 60px;">info@msemedikal.com</p>', '+903266184765');

CREATE TABLE `urunler` (
  `urun_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) NOT NULL,
  `urun_adi` text NOT NULL,
  `aciklama` text NOT NULL,
  `resim` text NOT NULL,
  `fiyat` double NOT NULL,
  PRIMARY KEY (`urun_id`),
  KEY `kategori_id` (`kategori_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin5 AUTO_INCREMENT=4 ;

INSERT INTO `urunler` (`urun_id`, `kategori_id`, `urun_adi`, `aciklama`, `resim`, `fiyat`) VALUES
(2, 2, 'Test ürün 1', 'Bu bir test ürünüdür.', 'http://localhost/mse-medikal/assets/images/6ea356aff30e188f7bef2a2b0fadde36.png', 0),
(3, 3, 'Test Ürün 2', 'Bu da başka bir test ürünü', 'http://localhost/mse-medikal/assets/images/82bfc3747885a6864e5ae72c6dbd807a.png', 0);

CREATE TABLE `yoneticiler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad` varchar(50) NOT NULL,
  `soyad` varchar(50) NOT NULL,
  `email` varchar(256) NOT NULL,
  `kullanici_adi` varchar(16) NOT NULL,
  `sifre` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin5 AUTO_INCREMENT=3 ;

INSERT INTO `yoneticiler` (`id`, `ad`, `soyad`, `email`, `kullanici_adi`, `sifre`) VALUES
(1, 'Sabit', 'Özdemir', 'sabit@gmail.com', 'sbtzdmr', 'c802e98ac6be49c36f26b458f6e3b195'),
(2, 'Mazlum', 'Geçgil', 'mazlum@gmail.com', 'mu7oo', 'c7160bd0332015b36ced03b3a38ade1a');


ALTER TABLE `urunler`
  ADD CONSTRAINT `urunler_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `kategoriler` (`kategori_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
