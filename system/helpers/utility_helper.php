<?php

if ( ! function_exists('asset_url'))
{
	function asset_url($asset = '', $type = '')
	{
		return base_url('assets/'.$type.'/'.$asset);
	}
}
