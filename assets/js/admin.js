function initTinyMCE() {        
    // tinymce initialization.
    tinymce.init({
        selector:'textarea#hakkimizda, textarea#vizyon, textarea#misyon, textarea#iletisim, textarea',
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ], 
        menubar: false, 
        statusbar: false,
        language: 'tr_TR',
        height: 200,
        width: "100%"
    });
    // ./tinymce init
}

function initCustomValidation() {
    // Custom validation
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Bu alan boş bırakılamaz!");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
    // ./custom
    
}

function misc() {
    // Hash links scroll animation
    var $root = $('html, body');
    $('a').click(function() {
        $root.animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        return false;
    });
    // ./hash    
}

function init() {
    initCustomValidation();
    
    initTinyMCE();
    
    misc();    
}

$(document).ready(init);