<?php
include 'MyController.php';

class Oturum_kapat extends MyController {
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        if($this->session->userdata('oturum')) {
            $this->session->sess_destroy();
            redirect('anasayfa', 'refresh');
        }
           redirect('anasayfa', 'refresh');
    }
}