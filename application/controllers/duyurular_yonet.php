<?php
include "MyController.php";

class Duyurular_yonet extends MyController {
    public function __construct() {
        parent::__construct();
        
        if(! $this->session->userdata('oturum')) {
            redirect('anasayfa');
        }
        $this->load->library("form_validation");
        $this->load->model('duyurular_model');
    }
    
    public function index() {
        redirect('duyurular_yonet/listele');
    }
    
    public function listele() {
        $data["duyurular"] = $this->duyurular_model->liste_getir();
        $this->load->view("admin/header", $data);
        $this->load->view("admin/listele/duyuru", $data);
        $this->load->view("admin/footer", $data);
    }
    
    public function ekle() {
        if($this->input->post("submit")) {
            $data = array(
                "baslik" => $this->input->post("baslik"),        
                "icerik" => $this->input->post("icerik"),        
                "tarih" => date("d.m.Y")        
                );
            $this->duyurular_model->ekle($data);
            redirect("duyurular_yonet/listele");
        } else {
            $this->load->view("admin/header");
            $this->load->view("admin/ekle/duyuru");
            $this->load->view("admin/footer");
        }
    }
    
    public function duzenle($id = 0) {
        if($id > 0) {
            if($this->input->post("submit")) {
                $data = array(
                    "baslik" => $this->input->post("baslik"),        
                    "icerik" => $this->input->post("icerik"),        
                    "tarih" => date("d.m.Y")        
                );
                $this->duyurular_model->duzenle($id, $data);
                redirect('duyurular_yonet/listele');
            } else {
                $data["duyuru"] = $this->duyurular_model->duyuru_getir($id);
                if($data["duyuru"]) {
                    $this->load->view("admin/header", $data);
                    $this->load->view("admin/duzenle/duyuru", $data);
                    $this->load->view("admin/footer");
                }
            }
        } else {
            redirect('admin/home');
        }
    }
    
    public function sil($id = 0) {
        if($id > 0) {
            $this->duyurular_model->sil($id);
        }
        redirect('duyurular_yonet/listele');
    }
}