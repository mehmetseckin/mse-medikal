<?php
include 'MyController.php';

class Hakkimizda extends MyController {
    public function __construct() {
        parent::__construct();
        $this->load->model("sabitler_model");
    }
    
    public function index() {
        $data["sayfa"] = "hakkimizda";
        $data["hakkimizda"] = $this->sabitler_model->hakkimizda();
        
        $this->load->view('include/header', $data);
        $this->load->view('hakkimizda', $data);
        $this->load->view('include/footer');
    }
}