<?php
include 'MyController.php';

class Duyurular extends MyController  {
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data["sayfa"] = "duyurular";
        
        $this->load->model('kategoriler_model');
        $data["kategoriler"] = $this->kategoriler_model->liste_getir();
        
        $this->load->model('duyurular_model');
        $data["duyurular"] = $this->duyurular_model->liste_getir();
        $this->load->view('include/header',$data);
        $this->load->view('duyurular', $data);
        $this->load->view('include/footer');
    }
    
    public function goruntule($id = 0) {
        if($id > 0 ) {
        $data["sayfa"] = "duyurular";
        $this->load->model('duyurular_model');
        $data["duyurular"] = $this->duyurular_model->liste_getir();
        $data["goster"] = $this->duyurular_model->duyuru_getir($id);
        $this->load->view('include/header',$data);
        $this->load->view('duyurular', $data);
        $this->load->view('include/footer');
        }
        else {
            redirect('duyurular');
        }
        
    }
}