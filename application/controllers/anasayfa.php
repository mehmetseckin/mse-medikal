<?php
include 'MyController.php';

class Anasayfa extends MyController  {
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->load->helper(array('form'));
        $data["sayfa"] = "anasayfa";
        $this->load->view('include/header',$data);
        $this->load->view('anasayfa');
        $this->load->view('include/footer');
        
    }
}