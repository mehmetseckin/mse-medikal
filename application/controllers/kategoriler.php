<?php
include "MyController.php";

class Kategoriler extends MyController {
    public function __construct() {
        parent::__construct();
        
        if(! $this->session->userdata('oturum')) {
            redirect('anasayfa');
        }
        $this->load->library("form_validation");
        $this->load->model('kategoriler_model');
    }
    
    public function index() {
        redirect('kategoriler/listele');
    }
    
    public function listele() {
        $data["kategoriler"] = $this->kategoriler_model->liste_getir();
        $this->load->view("admin/header", $data);
        $this->load->view("admin/listele/kategori", $data);
        $this->load->view("admin/footer", $data);
    }
    
    public function ekle() {
        if($this->input->post("submit")) {
            $data = array(
                "kategori_adi" => $this->input->post("kategori_adi"),
                "aciklama" => $this->input->post("aciklama")        
                );
            $this->kategoriler_model->ekle($data);
            redirect("kategoriler/listele");
        } else {
            $this->load->view("admin/header");
            $this->load->view("admin/ekle/kategori");
            $this->load->view("admin/footer");
        }
    }
    
    public function duzenle($id = 0) {
        if($id > 0) {
            if($this->input->post("submit")) {
                $data = array(
                    'kategori_adi' => $this->input->post("kategori_adi"),
                    'aciklama' => $this->input->post("aciklama")
                );
                $this->kategoriler_model->duzenle($id, $data);
                redirect('kategoriler/listele');
            } else {
                $data["kategori"] = $this->kategoriler_model->kategori_getir($id);
                if($data["kategori"]) {
                $this->load->view("admin/header", $data);
                $this->load->view("admin/duzenle/kategori", $data);
                $this->load->view("admin/footer");
                }
            }
        } else {
            redirect('admin/home');
        }
    }
    
    public function sil($id = 0) {
        if($id > 0) {
            $this->kategoriler_model->sil($id);
        }
        redirect('kategoriler/listele');
    }
}