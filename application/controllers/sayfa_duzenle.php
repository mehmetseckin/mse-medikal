<?php
include "MyController.php";

class Sayfa_duzenle extends MyController {
    public function __construct() {
        parent::__construct();
        if(! $this->session->userdata('oturum')) {
            redirect('anasayfa');
        }
        $this->load->library('form_validation');
        $this->load->model('sabitler_model');        
    }
    
    public function hakkimizda() {
        
        if($this->input->post("submit")) {
            $d = $this->input->post("hakkimizda");
            if(strlen($d) > 0) {
                $this->sabitler_model->hakkimizda($d);
            }
        } 
        $data['hakkimizda'] = $this->sabitler_model->hakkimizda();
        
        $this->load->view('admin/header');
        $this->load->view('admin/duzenle/hakkimizda', $data);
        $this->load->view('admin/footer');
    }
    
    public function vizyon_misyon() {
        
        if($this->input->post("submit")) {
            $d = $this->input->post("vizyon");
            $e = $this->input->post("misyon");
            if(strlen($d) > 0) {
                $this->sabitler_model->vizyon($d);
            }
            if(strlen($e) > 0) {
                $this->sabitler_model->misyon($e);
            }            
        } 
        $data['vizyon'] = $this->sabitler_model->vizyon();
        $data['misyon'] = $this->sabitler_model->misyon();
        
        $this->load->view('admin/header');
        $this->load->view('admin/duzenle/vizyon_misyon', $data);
        $this->load->view('admin/footer');
    }
    
    public function iletisim() {
        
        if($this->input->post("submit")) {
            $d = $this->input->post("iletisim");
            if(strlen($d) > 0) {
                $this->sabitler_model->iletisim($d);
            }
        } 
        $data['iletisim'] = $this->sabitler_model->iletisim();
        
        $this->load->view('admin/header');
        $this->load->view('admin/duzenle/iletisim', $data);
        $this->load->view('admin/footer');
    }
}
