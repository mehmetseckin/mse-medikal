<?php
include 'MyController.php';

class Oturum_ac extends MyController {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('yoneticiler_model');
    }
    
    public function index() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('kullanici_adi', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sifre', 'Password', 'trim|required|xss_clean|callback_oturum_ac');
        
        if($this->form_validation->run() == FALSE) {
            redirect("anasayfa", "refresh");   
        } else {
            redirect("anasayfa", "refresh");   
        }
    }
    
    public function oturum_ac($password) {
        $username = $this->input->post('kullanici_adi');
        $result = $this->yoneticiler_model->oturum_ac($username, $password);
        var_dump($result);
        if($result) {
            $sessionData = array (
                'id' => $result["id"],
                'ad' => $result["ad"],
                'soyad' => $result["soyad"],
                'email' => $result["email"]
            );
            $this->session->set_userdata('oturum', $sessionData);
            return TRUE;
        }
        else {
            $this->form_validation->set_message('oturum_ac', 'Kullanıcı adı - şifre kombinasyonunuz hatalı.');
            return FALSE;
        }
    }
}