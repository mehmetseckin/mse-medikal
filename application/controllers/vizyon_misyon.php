<?php
include 'MyController.php';

class Vizyon_misyon extends MyController {
    public function __construct() {
        parent::__construct();
        $this->load->model("sabitler_model");
    }
    
    public function index() {
        $data["sayfa"] = "vizyon_misyon";
        $data["vizyon"]=$this->sabitler_model->vizyon();
        $data["misyon"]=$this->sabitler_model->misyon();
        
        $this->load->view('include/header',$data);
        $this->load->view('vizyon_misyon', $data);
        $this->load->view('include/footer');
    }
}

