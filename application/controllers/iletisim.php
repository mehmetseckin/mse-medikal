<?php
include 'MyController.php';

class Iletisim extends MyController {
    public function __construct() {
        parent::__construct();
        $this->load->model("sabitler_model");
    }
    
    public function index() {
        $data["sayfa"] = "iletisim";
        $data["iletisim"]=$this->sabitler_model->iletisim();
        
        $this->load->view('include/header',$data);
        $this->load->view('iletisim', $data);
        $this->load->view('include/footer');
    }
}

