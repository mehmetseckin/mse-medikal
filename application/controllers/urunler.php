<?php
include "MyController.php";

class Urunler extends MyController {
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data["sayfa"] = "urunler";
        
        $this->load->model('kategoriler_model');
        $data["kategoriler"] = $this->kategoriler_model->liste_getir();
        
        $this->load->model('urunler_model');
        $data["urunler"] = $this->urunler_model->liste_getir();
        $this->load->view('include/header',$data);
        $this->load->view('urunler', $data);
        $this->load->view('include/footer');
    }
    
    public function kategori($id = 0) {
        if($id > 0 ) {
        $data["sayfa"] = "urunler";
        $this->load->model('kategoriler_model');
        $data["kategoriler"] = $this->kategoriler_model->liste_getir();
                   
        $this->load->model('urunler_model');
        $data["urunler"] = $this->urunler_model->liste_getir($id);
        $this->load->view('include/header',$data);
        $this->load->view('urunler', $data);
        $this->load->view('include/footer');
        }
        else {
            redirect('urunler');
        }
        
    }
}
