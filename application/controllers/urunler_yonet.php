<?php
include "MyController.php";

class Urunler_yonet extends MyController {
    public function __construct() {
        parent::__construct();
        
        if(! $this->session->userdata('oturum')) {
            redirect('anasayfa');
        }
        $this->load->library("form_validation");
        $this->load->model('urunler_model');
        $this->load->model('kategoriler_model');
        
        $config['upload_path'] = './assets/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '819600';
        $config['encrypt_name']	= TRUE;
        $this->load->library('upload', $config);
    }
    
    public function index() {
        redirect('urunler_yonet/listele');
    }
    
    public function listele() {
        $data["urunler"] = $this->urunler_model->liste_getir();
        $this->load->view("admin/header", $data);
        $this->load->view("admin/listele/urun", $data);
        $this->load->view("admin/footer", $data);
    }
    
    public function ekle() {
        if($this->input->post("submit")) {
            
            if($this->upload->do_upload("resim")) {
                $resim = $this->upload->data();
                $data = array(
                    "kategori_id" => $this->input->post("kategori_id"),
                    "urun_adi" => $this->input->post("urun_adi"),
                    "aciklama" => $this->input->post("aciklama"),
                    "resim" => asset_url($resim["file_name"], 'images'),
                    "fiyat" => $this->input->post("fiyat")        
                    );
                $this->urunler_model->ekle($data);
                redirect('urunler_yonet/listele');
            } else {
                echo $this->upload->display_errors();
            }
        } else {
            $data["kategoriler"] = $this->kategoriler_model->liste_getir();
            $this->load->view("admin/header");
            $this->load->view("admin/ekle/urun", $data);
            $this->load->view("admin/footer");
        }
    }
    
    public function duzenle($id = 0) {
        if($id > 0) {
            if($this->input->post("submit")) {
               
                $data = array(
                    'urun_adi' => $this->input->post("urun_adi"),
                    'aciklama' => $this->input->post("aciklama"),
                    'fiyat' => $this->input->post("fiyat")
                );
                
                if($this->upload->do_upload("resim")) {
                    $resim = $this->upload->data();
                    $data['resim'] = asset_url($resim["file_name"], 'images');
                }
                
                $this->urunler_model->duzenle($id, $data);
                redirect('urunler_yonet/listele');
            } else {
                
                $data["urun"] = $this->urunler_model->urun_getir($id);
                $data["kategoriler"] = $this->kategoriler_model->liste_getir();
                if($data["urun"]) {
                $this->load->view("admin/header", $data);
                $this->load->view("admin/duzenle/urun", $data);
                $this->load->view("admin/footer");
                }
            }
        } else {
            redirect('admin/home');
        }
    }
    
    public function sil($id = 0) {
        if($id > 0) {
            $this->urunler_model->sil($id);
        }
        redirect('urunler_yonet/listele');
    }
}