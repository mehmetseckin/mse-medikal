<?php
include "MyController.php";

class Admin extends MyController {
    
    public function __construct() {
        parent::__construct();
        if(!$this->session->userdata('oturum')) redirect('anasayfa');
    }
    
    public function index() {
        redirect('urunler_yonet/listele');
    }    
    
    public function home() {
        $this->load->view('admin/header');
        $this->load->view('admin/home');
        $this->load->view('admin/footer');        
    }
}