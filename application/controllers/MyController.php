<?php
class MyController extends CI_Controller {
    
    protected $oturum;
    protected $telefon;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('sabitler_model');
        $data = new stdClass();
        $data->oturum = $this->session->userdata('oturum');                
        $data->telefon = $this->sabitler_model->telefon();
        $this->load->vars($data);
    }
    
    
}