
<div class="page-container">  
    <center>
    <?php
    if(isset($iletisim)) {
        echo $iletisim;
    }
?>
    </center>
        <div class="wrap">
            <form class="form-horizontal">
            <fieldset>
             <legend>Bize Ulaşın.</legend>
               
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="isim">İsim</label>  
              <div class="col-md-5">
              <input id="isim" name="isim" type="text" placeholder="İsminiz" class="form-control input-md" required="">
              <span class="help-block">Lütfen gerçek isminizi yazınız.</span>  
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="email">E-Mail</label>  
              <div class="col-md-5">
              <input id="email" name="email" type="text" placeholder="E-Posta adresiniz" class="form-control input-md" required="">
              <span class="help-block">Size ulaşabileceğimiz e-mail adresinizi yazınız.</span>  
              </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="mesaj">Mesajınız</label>
              <div class="col-md-5">                     
                <textarea class="form-control" id="mesaj" name="mesaj" cols="70" rows="5"></textarea>
              </div>
            </div>

            <!-- Button -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="submit"></label>
              <div class="col-md-5">
                <button id="submit" name="submit" class="btn btn-primary float-rt">Gönder</button>
              </div>
            </div>

            </fieldset>
            </form>
        </div>
      </div>
