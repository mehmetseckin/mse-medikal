<div class="content">
				<div class="wrap">
					<div class="services">
						
						<div class="services-sidebar">
                                                    <?php if($kategoriler) { ?>
							<h3>Kategoriler</h3>
							 <ul>
							    <?php foreach($kategoriler as $kategori) { ?>
                                                             <li>
                                                                 <a href="<?=base_url('urunler/kategori/'.$kategori["kategori_id"])?>"><?=$kategori["kategori_adi"]?></a>
                                                                 <p><small><?=$kategori["aciklama"]?></small></p>
                                                             </li>
                                                            <?php } ?>
					 		 </ul>
                                                    <?php } ?>
						</div>
						<div class="service-content">
                                                    <?php if($urunler) { ?>
							<h3>Ürünler</h3>
							<?php foreach($urunler as $urun) { ?>
                                                        <ul>                                                            
								<li>
                                                                        <img src="<?=$urun["resim"]?>" width="75" height="75" style="border-radius: 35px; float: left;">
                                                                   
                                                                </li>
                                                                <li>
                                                                    <p style="width: 90%;">
                                                                        <a href=""><?=$urun["urun_adi"]?></a>
                                                                        <?=$urun["aciklama"]?>
                                                                    </p>
                                                                </li>
                                                                <div class="clear"> </div>
                                                        </ul>
                                                        <?php } ?>
                                                    <?php } ?>
						</div><div class="clear"> </div>
					</div>
				<div class="clear"> </div>
				</div>
			<!----End-content----->
		</div>