<!DOCTYPE HTML>
<html>
	<head>
                <meta charset="utf-8"> 
		<title>MSE Medical</title>
		<link href="<?php echo asset_url("style.css", "css"); ?>" rel="stylesheet" type="text/css"  media="all" />
		<link href="<?php echo asset_url("bootstrap.css", "css"); ?>" rel="stylesheet" type="text/css"  media="all" />
		<link href="<?php echo asset_url("bootstrap-theme.min.css", "css"); ?>" rel="stylesheet" type="text/css"  media="all" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo asset_url("responsiveslides.css", "css"); ?>">
                <link rel="shortcut icon" href="<?php echo asset_url("favicon.ico", "images"); ?>">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="<?php echo asset_url("responsiveslides.min.js", "js"); ?>"></script>
		<script src="<?php echo asset_url("bootstrap.min.js", "js"); ?>"></script>
		<script src="<?php echo asset_url("login.js", "js"); ?>"></script>
		  <script>
		    // You can also use "$(window).load(function() {"
			    $(function () {
			      // Slideshow 1
			      $("#slider1").responsiveSlides({
			        maxwidth: 2500,
			        speed: 600
			      });
			});
		  </script>
	</head>
	<body>
		<!---start-wrap---->
		                        <div class="main-container">
			<!---start-header---->
			<div class="header">
					<div class="top-header">
						<div class="wrap">
						
                                            <div class="top-header-left">
							<p><?=$telefon?></p>
						</div>
						<div class="right-left">
							<ul>
                                                            <?php if(isset($oturum) && $oturum) { ?>
                                                                <li>Hoşgeldiniz, <?=$oturum["ad"]?> <?=$oturum["soyad"]?></li>
                                                                <li class="divider">|</li>
                                                                <li class="sign"><a href="<?=base_url('admin/index')?>">Yönetim Paneli</a></li>
                                                                <li class="divider">|</li>                                                                
                                                                <li class="login"><a href="<?=base_url('oturum_kapat')?>">Oturumu kapat</a></li>
                                                            <?php } else { ?>
								<li class="login" id="login-link"><a href="#">Giriş</a></li>
                                                                <?php echo validation_errors(); ?>                                                                
                                                                <div class="login-form-container">
                                                                 <?php echo form_open('oturum_ac', array('class' => 'form-horizontal')); ?>
                                                                    <fieldset>
                                                                        <input id="kullanici_adi" name="kullanici_adi" type="text" placeholder="Kullanıcı Adı" class="form-control input-md">
                                                                        <input id="sifre" name="sifre" type="password" placeholder="Şifre" class="form-control input-md">
                                                                        <input type="submit" value="Giriş Yap" id="login" name="login" class="btn-sm btn-primary" />
                                                                    </fieldset>
                                                                    </form>
                                                                </div>
                                                            <?php } ?>
							</ul>
						</div>
						<div class="clear"> </div>
					</div>
				</div>
					<div class="main-header">
						<div class="wrap">
							<div class="logo">
								<a href="<?=base_url()?>"><img src="<?php echo asset_url('logo.png','images'); ?>" title="logo" /></a>
							</div>
						
							<div class="clear"> </div>
						</div>
					</div>
					<div class="clear"> </div>
					<div class="top-nav">
						<div class="wrap">
							<ul>
								<li <?php if($sayfa=="anasayfa") echo 'class="active"'; ?>><a href="<?php echo base_url("anasayfa"); ?>">Anasayfa</a></li>
								<li <?php if($sayfa=="hakkimizda") echo 'class="active"'; ?>><a href="<?php echo base_url("hakkimizda"); ?>">Hakkımızda</a></li>
								<li <?php if($sayfa=="vizyon_misyon") echo 'class="active"'; ?>><a href="<?php echo base_url("vizyon_misyon"); ?>">Vİzyon-Mİsyon</a></li>
								<li <?php if($sayfa=="urunler") echo 'class="active"'; ?>><a href="<?php echo base_url("urunler"); ?>">Ürünler</a></li>
								<li <?php if($sayfa=="duyurular") echo 'class="active"'; ?>><a href="<?php echo base_url("duyurular"); ?>">Duyurular</a></li>
								<li <?php if($sayfa=="iletisim") echo 'class="active"'; ?>><a href="<?php echo base_url("iletisim"); ?>">İletİşİm</a></li>
								<div class="clear"> </div>
							</ul>
						</div>
					</div>
			</div>
			<!---End-header---->
