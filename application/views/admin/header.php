<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">        
        <title>MSE Medikal | Admin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="shortcut icon" href="<?php echo asset_url('favicon.ico', 'images'); ?>">
        <link rel="stylesheet" href="<?php echo asset_url('reset.css', 'css'); ?>" />        
        <link rel="stylesheet" href="<?php echo asset_url('bootstrap.css', 'css'); ?>" />
        <link rel="stylesheet" href="<?php echo asset_url('bootstrap-switch.css', 'css'); ?>" />
        <link rel="stylesheet" href="<?php echo asset_url('admin.css', 'css'); ?>" />           
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="main-container">
            <link  rel="stylesheet" href="<?php echo asset_url('admin.css', 'css'); ?>">
            <div class="container">
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                  <div class="container-fluid">
                    <div class="navbar-header">
                      <a class="navbar-brand" href="<?php echo base_url('admin/home/'); ?>">Yönetim Paneli</a>
                    </div>
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url(); ?>" target="_blank"><span class="glyphicon glyphicon-home"></span> Anasayfa</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-gift"></span> Ürünler<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=base_url('urunler_yonet/ekle')?>"><span class="glyphicon glyphicon-plus-sign"></span> Ürün ekle</a></li>
                              <li class="divider"></li>                              
                              <li><a href="<?=base_url('urunler_yonet/listele')?>"><span class="glyphicon glyphicon-list"></span> Ürünleri listele</a></li>
                            </ul>
                          </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tasks"></span> Kategoriler<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?=base_url('kategoriler/ekle')?>"><span class="glyphicon glyphicon-plus-sign"></span> Kategori ekle</a></li>
                              <li class="divider"></li>                              
                              <li><a href="<?=base_url('kategoriler/listele')?>"><span class="glyphicon glyphicon-list"></span> Kategorileri listele</a></li>
                            </ul>
                        </li>   
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-comment"></span> Duyurular<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?=base_url('duyurular_yonet/ekle')?>"><span class="glyphicon glyphicon-plus-sign"></span> Duyuru ekle</a></li>
                              <li class="divider"></li>                              
                              <li><a href="<?=base_url('duyurular_yonet/listele')?>"><span class="glyphicon glyphicon-list"></span> Duyuruları listele</a></li>
                            </ul>
                        </li>                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-edit"></span> Sayfaları Düzenle<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?php echo base_url('sayfa_duzenle/hakkimizda'); ?>">
                                      <span class="glyphicon glyphicon-edit"></span> Hakkımızda</a></li>
                              <li class="divider"></li>                              
                              <li><a href="<?php echo base_url('sayfa_duzenle/vizyon_misyon'); ?>">
                                      <span class="glyphicon glyphicon-edit"></span> Vizyon - Misyon</a></li>
                              <li class="divider"></li>                              
                              <li><a href="<?php echo base_url('sayfa_duzenle/iletisim'); ?>">
                                      <span class="glyphicon glyphicon-edit"></span> İletişim</a></li>
                            </ul>
                        </li>                            
                        <li><a href="<?php echo base_url('oturum_kapat'); ?>"><span class="glyphicon glyphicon-log-out"></span> Çıkış</a></li>
                      </ul>          
                  </div>
                </div>