<div class="container push-footer">
<?php echo form_open(base_url('duyurular_yonet/ekle'), array("class" => "form-horizontal")); ?>
<fieldset>

<!-- Form Name -->
<legend>Duyuru Ekle</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="baslik">Başlık</label>  
  <div class="col-md-5">
  <input id="baslik" name="baslik" type="text" placeholder="Yeni Duyuru" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="icerik">İçerik</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="icerik" name="icerik"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Kaydet">
  </div>
</div>

</fieldset>
</form>
