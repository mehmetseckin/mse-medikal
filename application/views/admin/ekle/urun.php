<div class="container push-footer">
<?php echo form_open_multipart('', array("class" => "form-horizontal")); ?>

    <fieldset>

<!-- Form Name -->
<legend>Ürün Ekle</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="urun_adi">Ürün Adı</label>  
  <div class="col-md-5">
  <input id="urun_adi" name="urun_adi" type="text" placeholder="Yeni Ürün" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">Açıklama</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="aciklama" name="aciklama"></textarea>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori_id">Kategori</label>
  <div class="col-md-4">
    <select id="kategori_id" name="kategori_id" class="form-control">
      <?php foreach ($kategoriler as $kategori) { ?>
      <option value="<?=$kategori["kategori_id"]?>"><?=$kategori["kategori_adi"]?></option>
      <?php } ?>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="fiyat">Fiyat</label>  
  <div class="col-md-5">
  <input id="fiyat" name="fiyat" type="text" placeholder="$0.99" class="form-control input-md" required="">
    
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="resim">Resim</label>
  <div class="col-md-4">
    <input id="resim" name="resim" class="input-file" type="file">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Kaydet">
  </div>
</div>
</fieldset>
</form>
