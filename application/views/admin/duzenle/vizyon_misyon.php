<?php echo form_open('sayfa_duzenle/vizyon_misyon', array("class" => "form-horizontal")); ?>
<fieldset>

<!-- Form Name -->
<legend>Vizyon & Misyon Sayfasını Düzenle</legend>

<!-- Textarea -->
<div class="form-group">
  <div class="col-md-12">                     
    <textarea class="form-control" id="vizyon" name="vizyon"><?php echo $vizyon ?></textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <div class="col-md-12">                     
    <textarea class="form-control" id="misyon" name="misyon"><?php echo $misyon ?></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <div class="col-md-4">
    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Kaydet">
  </div>
</div>

</fieldset>
</form>