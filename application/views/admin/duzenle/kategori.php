<div class="container push-footer">
<?php echo form_open('', array("class" => "form-horizontal")); ?>
<fieldset>
<!-- Form Name -->
<legend>Kategori Düzenle</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori_adi">Kategori Adı</label>  
  <div class="col-md-5">
  <input id="kategori_adi" name="kategori_adi" type="text" placeholder="Yeni Kategori" class="form-control input-md" required="" value="<?=$kategori["kategori_adi"]?>">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">Açıklama</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="aciklama" name="aciklama"><?=$kategori["aciklama"]?></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Kaydet">
  </div>
</div>

</fieldset>
</form>