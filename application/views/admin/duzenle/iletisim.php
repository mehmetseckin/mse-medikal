<?php echo form_open('sayfa_duzenle/iletisim', array("class" => "form-horizontal")); ?>
<fieldset>

<!-- Form Name -->
<legend>İletişim Sayfasını Düzenle</legend>

<!-- Textarea -->
<div class="form-group">
  <div class="col-md-12">                     
    <textarea class="form-control" id="iletisim" name="iletisim"><?php echo $iletisim ?></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <div class="col-md-4">
    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Kaydet">
  </div>
</div>

</fieldset>
</form>