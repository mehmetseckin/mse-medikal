<div class="container push-footer">
    <h3>Ürün Listesi</h3>
    <hr class="divider">
    <table class="table table-striped">
        <thead>
          <tr>
              <th class="col-sm-1"></th>
            <th class="col-sm-2">Kategori</th>
            <th class="col-sm-3">Ürün Adı</th>
            <th class="col-sm-4">Açıklama</th>
            <th class="col-sm-2">İşlem</th>
          </tr>
        </thead>
        <tbody>
            <?php 
            if($urunler) { 
                foreach($urunler as $urun) {
                    echo '<tr>';
                    echo '<td><img src="'. $urun["resim"] . '" width="75" height="75" style="border-radius: 35px;"></td>';
                    echo '<td>' . $urun["kategori_adi"] . '</td>';
                    echo '<td>' . $urun["urun_adi"] . '</td>';
                    echo '<td>' . $urun["aciklama"] . '</td>';
                    echo '<td>';
                    echo '<a href="'. base_url('urunler_yonet/duzenle/'.$urun["urun_id"]) .'" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span> Düzenle</a>';
                    echo ' <a href="'. base_url('urunler_yonet/sil/'.$urun["urun_id"]) .'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Sil</a>';
                    echo '</td>';
                    echo '</tr>';
                }  
            } else { ?>
            <td colspan="4" align="center">Henüz içerik eklenmemiş!</td>
            <?php } ?>
        </tbody>
      </table>