<div class="container push-footer">
    <h3>Kategori Listesi</h3>
    <hr class="divider">
    <table class="table table-striped">
        <thead>
          <tr>
            <th class="col-sm-4">Kategori Adı</th>
            <th class="col-sm-6">Açıklama</th>
            <th class="col-sm-2">İşlem</th>
          </tr>
        </thead>
        <tbody>
            <?php 
            if($kategoriler) { 
                foreach($kategoriler as $item) {
                    echo '<tr>';
                    echo '<td>' . $item["kategori_adi"] . '</td>';
                    echo '<td>' . $item["aciklama"] . '</td>';
                    echo '<td>';
                    echo '<a href="'. base_url('kategoriler/duzenle/'.$item["kategori_id"]) .'" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span> Düzenle</a>';
                    echo ' <a href="'. base_url('kategoriler/sil/'.$item["kategori_id"]) .'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Sil</a>';
                    echo '</td>';
                    echo '</tr>';
                }  
            } else { ?>
            <td colspan="4" align="center">Henüz içerik eklenmemiş!</td>
            <?php } ?>
        </tbody>
      </table>