<div class="container push-footer">
    <h3>Duyuru Listesi</h3>
    <hr class="divider">
    <table class="table table-striped">
        <thead>
          <tr>
            <th class="col-sm-4">Tarih</th>
            <th class="col-sm-6">Başlık</th>
            <th class="col-sm-2">İşlem</th>
          </tr>
        </thead>
        <tbody>
            <?php 
            if($duyurular) { 
                foreach($duyurular as $item) {
                    echo '<tr>';
                    echo '<td>' . $item["tarih"] . '</td>';
                    echo '<td>' . $item["baslik"] . '</td>';
                    echo '<td>';
                    echo '<a href="'. base_url('duyurular_yonet/duzenle/'.$item["duyuru_id"]) .'" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span> Düzenle</a>';
                    echo ' <a href="'. base_url('duyurular_yonet/sil/'.$item["duyuru_id"]) .'" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Sil</a>';
                    echo '</td>';
                    echo '</tr>';
                }  
            } else { ?>
            <td colspan="4" align="center">Henüz içerik eklenmemiş!</td>
            <?php } ?>
        </tbody>
      </table>