<?php

class Kategoriler_model extends CI_Model {
    private $table = "kategoriler";
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function kategori_getir($id) {
        $query = $this->db->get_where($this->table, array('kategori_id' => $id));
        return $query->row_array();
    }
    
    public function ekle($data = null) {
        if(is_array($data)) {
            $this->db->insert($this->table, $data);
        }
    }
    
    public function duzenle($id, $data = null) {
        if(is_array($data)) {
            $this->db->where('kategori_id', $id);
            $this->db->update($this->table, $data);
        }
    }
    
    public function sil($id) {
        $this->db->delete($this->table, array('kategori_id' => $id));
    }
    
    public function liste_getir() {
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
}