<?php

class Yoneticiler_model extends CI_Model {
    private $table = "yoneticiler";
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function yonetici_getir($data) {
        $query = $this->db->get_where($this->table, $data);
        return $query->row_array();
    }
    
    public function oturum_ac($username, $password) {
        $this -> db -> select('id, ad, soyad, email');
        $this -> db -> from($this->table);
        $this -> db -> where('kullanici_adi', $username);
        $this -> db -> where('sifre', md5($password));
        $this -> db -> limit(1);
        
        $query = $this -> db -> get();
        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    
}