<?php

class Sabitler_model extends CI_Model {

    private $tablo = "sabitler";

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function hakkimizda($hakkimizda = null) {
        if($hakkimizda) {
            $this->db->set(array('hakkimizda' => $hakkimizda));
            $this->db->update($this->tablo);
        }
        $this->db->select('hakkimizda');
        $query = $this->db->get($this->tablo);
        $row = $query->row_array();
        return $row["hakkimizda"];
    }

    public function iletisim($iletisim = null) {
        if($iletisim) {
            $this->db->set(array('iletisim' => $iletisim));
            $this->db->update($this->tablo);
        }        
        $this->db->select('iletisim');
        $query = $this->db->get($this->tablo);
        $row = $query->row_array();
        return $row["iletisim"];
    }

    public function misyon($misyon = null) {
        if($misyon) {
            $this->db->set(array('misyon' => $misyon));
            $this->db->update($this->tablo);
        }        
        $this->db->select('misyon');
        $query = $this->db->get($this->tablo);
        $row = $query->row_array();
        return $row["misyon"];
    }

    public function vizyon($vizyon = null) {
        if($vizyon) {
            $this->db->set(array('vizyon' => $vizyon));
            $this->db->update($this->tablo);
        }           
        $this->db->select('vizyon');
        $query = $this->db->get($this->tablo);
        $row = $query->row_array();
        return $row["vizyon"];
    }

    public function telefon($telefon = null) {
        if($telefon) {
            $this->db->set(array('telefon' => $telefon));
            $this->db->update($this->tablo);
        }        
        $this->db->select('telefon');
        $query = $this->db->get($this->tablo);
        $row = $query->row_array();
        return $row["telefon"];
    }

}
