<?php

class Urunler_model extends CI_Model {
    private $table = "urunler";
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function urun_getir($id) {
        $this->db->select('u.urun_id, u.urun_adi, u.aciklama, u.resim, u.fiyat, k.kategori_adi, k.aciklama as "k_aciklama", k.kategori_id');
        $this->db->from("$this->table u, kategoriler k");
        $this->db->where('urun_id', $id);
        $this->db->where('u.kategori_id = k.kategori_id');
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function liste_getir($kategori_id = FALSE) {
        if($kategori_id === FALSE ) {
            $this->db->select('u.urun_id, u.urun_adi, u.aciklama, u.resim, u.fiyat, k.kategori_adi, k.aciklama as "k_aciklama", k.kategori_id');
            $this->db->from("$this->table u, kategoriler k");
            $this->db->where('u.kategori_id = k.kategori_id');            
            $query = $this->db->get();
        } else {
            $this->db->select('u.urun_id, u.urun_adi, u.aciklama, u.resim, u.fiyat, k.kategori_adi, k.aciklama as "k_aciklama", k.kategori_id');
            $this->db->from("$this->table u, kategoriler k");
            $this->db->where('u.kategori_id = k.kategori_id');    
            $this->db->where('u.kategori_id', $kategori_id);            
            $query = $this->db->get();
        }
        
        return $query->result_array();
    }
    
    public function ekle($data = null) {
        if(is_array($data)) {
            $this->db->insert($this->table, $data);
        }
    }
    
    public function duzenle($id, $data) {
        $this->db->where('urun_id', $id);
        $this->db->update($this->table, $data);
    }
    
    public function sil($id) {
        $this->db->delete($this->table, array('urun_id' => $id));
    }
}